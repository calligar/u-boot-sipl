// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2014 - 2015 Xilinx, Inc.
 * Michal Simek <michal.simek@xilinx.com>
 */

#include <common.h>
#include <uboot_aes.h>
#include <sata.h>
#include <ahci.h>
#include <scsi.h>
#include <malloc.h>
#include <wdt.h>
#include <asm/arch/clk.h>
#include <asm/arch/hardware.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/psu_init_gpl.h>
#include <asm/io.h>
#include <dm/device.h>
#include <dm/uclass.h>
#include <usb.h>
#include <dwc3-uboot.h>
#include <zynqmppl.h>
#include <g_dnl.h>
#include <i2c.h>
#include <net.h>
#include <serial.h>
#include <dm/device-internal.h>
#include <sipl.h>

DECLARE_GLOBAL_DATA_PTR;

#if !defined(CONFIG_SPL_BUILD) && defined(CONFIG_WDT)
static struct udevice *watchdog_dev;
#endif

#if defined(CONFIG_FPGA) && defined(CONFIG_FPGA_ZYNQMPPL) && \
    !defined(CONFIG_SPL_BUILD)
static xilinx_desc zynqmppl = XILINX_ZYNQMP_DESC;

static const struct {
	u32 id;
	u32 ver;
	char *name;
	bool evexists;
} zynqmp_devices[] = {
	{
		.id = 0x10,
		.name = "3eg",
	},
	{
		.id = 0x10,
		.ver = 0x2c,
		.name = "3cg",
	},
	{
		.id = 0x11,
		.name = "2eg",
	},
	{
		.id = 0x11,
		.ver = 0x2c,
		.name = "2cg",
	},
	{
		.id = 0x20,
		.name = "5ev",
		.evexists = 1,
	},
	{
		.id = 0x20,
		.ver = 0x100,
		.name = "5eg",
		.evexists = 1,
	},
	{
		.id = 0x20,
		.ver = 0x12c,
		.name = "5cg",
		.evexists = 1,
	},
	{
		.id = 0x21,
		.name = "4ev",
		.evexists = 1,
	},
	{
		.id = 0x21,
		.ver = 0x100,
		.name = "4eg",
		.evexists = 1,
	},
	{
		.id = 0x21,
		.ver = 0x12c,
		.name = "4cg",
		.evexists = 1,
	},
	{
		.id = 0x30,
		.name = "7ev",
		.evexists = 1,
	},
	{
		.id = 0x30,
		.ver = 0x100,
		.name = "7eg",
		.evexists = 1,
	},
	{
		.id = 0x30,
		.ver = 0x12c,
		.name = "7cg",
		.evexists = 1,
	},
	{
		.id = 0x38,
		.name = "9eg",
	},
	{
		.id = 0x38,
		.ver = 0x2c,
		.name = "9cg",
	},
	{
		.id = 0x39,
		.name = "6eg",
	},
	{
		.id = 0x39,
		.ver = 0x2c,
		.name = "6cg",
	},
	{
		.id = 0x40,
		.name = "11eg",
	},
	{ /* For testing purpose only */
		.id = 0x50,
		.ver = 0x2c,
		.name = "15cg",
	},
	{
		.id = 0x50,
		.name = "15eg",
	},
	{
		.id = 0x58,
		.name = "19eg",
	},
	{
		.id = 0x59,
		.name = "17eg",
	},
	{
		.id = 0x61,
		.name = "21dr",
	},
	{
		.id = 0x63,
		.name = "23dr",
	},
	{
		.id = 0x65,
		.name = "25dr",
	},
	{
		.id = 0x64,
		.name = "27dr",
	},
	{
		.id = 0x60,
		.name = "28dr",
	},
	{
		.id = 0x62,
		.name = "29dr",
	},
	{
		.id = 0x66,
		.name = "39dr",
	},
};
#endif

int chip_id(unsigned char id)
{
	struct pt_regs regs;
	int val = -EINVAL;

	if (current_el() != 3) {
		regs.regs[0] = ZYNQMP_SIP_SVC_CSU_DMA_CHIPID;
		regs.regs[1] = 0;
		regs.regs[2] = 0;
		regs.regs[3] = 0;

		smc_call(&regs);

		/*
		 * SMC returns:
		 * regs[0][31:0]  = status of the operation
		 * regs[0][63:32] = CSU.IDCODE register
		 * regs[1][31:0]  = CSU.version register
		 * regs[1][63:32] = CSU.IDCODE2 register
		 */
		switch (id) {
		case IDCODE:
			regs.regs[0] = upper_32_bits(regs.regs[0]);
			regs.regs[0] &= ZYNQMP_CSU_IDCODE_DEVICE_CODE_MASK |
					ZYNQMP_CSU_IDCODE_SVD_MASK;
			regs.regs[0] >>= ZYNQMP_CSU_IDCODE_SVD_SHIFT;
			val = regs.regs[0];
			break;
		case VERSION:
			regs.regs[1] = lower_32_bits(regs.regs[1]);
			regs.regs[1] &= ZYNQMP_CSU_SILICON_VER_MASK;
			val = regs.regs[1];
			break;
		case IDCODE2:
			regs.regs[1] = lower_32_bits(regs.regs[1]);
			regs.regs[1] >>= ZYNQMP_CSU_VERSION_EMPTY_SHIFT;
			val = regs.regs[1];
			break;
		default:
			printf("%s, Invalid Req:0x%x\n", __func__, id);
		}
	} else {
		switch (id) {
		case IDCODE:
			val = readl(ZYNQMP_CSU_IDCODE_ADDR);
			val &= ZYNQMP_CSU_IDCODE_DEVICE_CODE_MASK |
			       ZYNQMP_CSU_IDCODE_SVD_MASK;
			val >>= ZYNQMP_CSU_IDCODE_SVD_SHIFT;
			break;
		case VERSION:
			val = readl(ZYNQMP_CSU_VER_ADDR);
			val &= ZYNQMP_CSU_SILICON_VER_MASK;
			break;
		default:
			printf("%s, Invalid Req:0x%x\n", __func__, id);
		}
	}

	return val;
}

#define ZYNQMP_VERSION_SIZE		9
#define ZYNQMP_PL_STATUS_BIT		9
#define ZYNQMP_IPDIS_VCU_BIT		8
#define ZYNQMP_PL_STATUS_MASK		BIT(ZYNQMP_PL_STATUS_BIT)
#define ZYNQMP_CSU_VERSION_MASK		~(ZYNQMP_PL_STATUS_MASK)
#define ZYNQMP_CSU_VCUDIS_VER_MASK	ZYNQMP_CSU_VERSION_MASK & \
					~BIT(ZYNQMP_IPDIS_VCU_BIT)
#define MAX_VARIANTS_EV			3

#if defined(CONFIG_FPGA) && defined(CONFIG_FPGA_ZYNQMPPL) && \
	!defined(CONFIG_SPL_BUILD)
static char *zynqmp_get_silicon_idcode_name(void)
{
	u32 i, id, ver, j;
	char *buf;
	static char name[ZYNQMP_VERSION_SIZE];

	id = chip_id(IDCODE);
	ver = chip_id(IDCODE2);

	for (i = 0; i < ARRAY_SIZE(zynqmp_devices); i++) {
		if (zynqmp_devices[i].id == id) {
			if (zynqmp_devices[i].evexists &&
			    !(ver & ZYNQMP_PL_STATUS_MASK))
				break;
			if (zynqmp_devices[i].ver == (ver &
			    ZYNQMP_CSU_VERSION_MASK))
				break;
		}
	}

	if (i >= ARRAY_SIZE(zynqmp_devices))
		return "unknown";

	strncat(name, "zu", 2);
	if (!zynqmp_devices[i].evexists ||
	    (ver & ZYNQMP_PL_STATUS_MASK)) {
		strncat(name, zynqmp_devices[i].name,
			ZYNQMP_VERSION_SIZE - 3);
		return name;
	}

	/*
	 * Here we are means, PL not powered up and ev variant
	 * exists. So, we need to ignore VCU disable bit(8) in
	 * version and findout if its CG or EG/EV variant.
	 */
	for (j = 0; j < MAX_VARIANTS_EV; j++, i++) {
		if ((zynqmp_devices[i].ver & ~BIT(ZYNQMP_IPDIS_VCU_BIT)) ==
		    (ver & ZYNQMP_CSU_VCUDIS_VER_MASK)) {
			strncat(name, zynqmp_devices[i].name,
				ZYNQMP_VERSION_SIZE - 3);
			break;
		}
	}

	if (j >= MAX_VARIANTS_EV)
		return "unknown";

	if (strstr(name, "eg") || strstr(name, "ev")) {
		buf = strstr(name, "e");
		*buf = '\0';
	}

	return name;
}
#endif

int board_early_init_f(void)
{
	int ret = 0;
#if !defined(CONFIG_SPL_BUILD) && defined(CONFIG_CLK_ZYNQMP)
	u32 pm_api_version;

	pm_api_version = zynqmp_pmufw_version();
	printf("PMUFW:\tv%d.%d\n",
	       pm_api_version >> ZYNQMP_PM_VERSION_MAJOR_SHIFT,
	       pm_api_version & ZYNQMP_PM_VERSION_MINOR_MASK);

	if (pm_api_version < ZYNQMP_PM_VERSION)
		panic("PMUFW version error. Expected: v%d.%d\n",
		      ZYNQMP_PM_VERSION_MAJOR, ZYNQMP_PM_VERSION_MINOR);
#endif

#if defined(CONFIG_ZYNQMP_PSU_INIT_ENABLED)
	ret = psu_init();
#endif

#if defined(CONFIG_WDT) && !defined(CONFIG_SPL_BUILD)
	/* bss is not cleared at time when watchdog_reset() is called */
	watchdog_dev = NULL;
#endif

	return ret;
}

int board_init(void)
{
	printf("EL Level:\tEL%d\n", current_el());

#if defined(CONFIG_FPGA) && defined(CONFIG_FPGA_ZYNQMPPL) && \
    !defined(CONFIG_SPL_BUILD) || (defined(CONFIG_SPL_FPGA_SUPPORT) && \
    defined(CONFIG_SPL_BUILD))
	if (current_el() != 3) {
		zynqmppl.name = zynqmp_get_silicon_idcode_name();
		printf("Chip ID:\t%s\n", zynqmppl.name);
		fpga_init();
		fpga_add(fpga_xilinx, &zynqmppl);
	}
#endif

#if !defined(CONFIG_SPL_BUILD) && defined(CONFIG_WDT)
	if (uclass_get_device_by_seq(UCLASS_WDT, 0, &watchdog_dev)) {
		debug("Watchdog: Not found by seq!\n");
		if (uclass_get_device(UCLASS_WDT, 0, &watchdog_dev)) {
			puts("Watchdog: Not found!\n");
			return 0;
		}
	}

	wdt_start(watchdog_dev, 0, 0);
	puts("Watchdog: Started\n");
#endif

	return 0;
}

#ifdef CONFIG_WATCHDOG
/* Called by macro WATCHDOG_RESET */
void watchdog_reset(void)
{
# if !defined(CONFIG_SPL_BUILD)
	static ulong next_reset;
	ulong now;

	if (!watchdog_dev)
		return;

	now = timer_get_us();

	/* Do not reset the watchdog too often */
	if (now > next_reset) {
		wdt_reset(watchdog_dev);
		next_reset = now + 1000;
	}
# endif
}
#endif

int board_early_init_r(void)
{
	u32 val;

	if (current_el() != 3)
		return 0;

	val = readl(&crlapb_base->timestamp_ref_ctrl);
	val &= ZYNQMP_CRL_APB_TIMESTAMP_REF_CTRL_CLKACT;

	if (!val) {
		val = readl(&crlapb_base->timestamp_ref_ctrl);
		val |= ZYNQMP_CRL_APB_TIMESTAMP_REF_CTRL_CLKACT;
		writel(val, &crlapb_base->timestamp_ref_ctrl);

		/* Program freq register in System counter */
		writel(zynqmp_get_system_timer_freq(),
		       &iou_scntr_secure->base_frequency_id_register);
		/* And enable system counter */
		writel(ZYNQMP_IOU_SCNTR_COUNTER_CONTROL_REGISTER_EN,
		       &iou_scntr_secure->counter_control_register);
	}
	return 0;
}

unsigned long do_go_exec(ulong (*entry)(int, char * const []), int argc,
			 char * const argv[])
{
	int ret = 0;

	if (current_el() > 1) {
		smp_kick_all_cpus();
		dcache_disable();
		armv8_switch_to_el1(0x0, 0, 0, 0, (unsigned long)entry,
				    ES_TO_AARCH64);
	} else {
		printf("FAIL: current EL is not above EL1\n");
		ret = EINVAL;
	}
	return ret;
}

#if !defined(CONFIG_SYS_SDRAM_BASE) && !defined(CONFIG_SYS_SDRAM_SIZE)
int dram_init_banksize(void)
{
	int ret;

	ret = fdtdec_setup_memory_banksize();
	if (ret)
		return ret;

	mem_map_fill();

	return 0;
}

int dram_init(void)
{
	if (fdtdec_setup_mem_size_base() != 0)
		return -EINVAL;

	return 0;
}
#else
int dram_init_banksize(void)
{
#if defined(CONFIG_NR_DRAM_BANKS)
	gd->bd->bi_dram[0].start = CONFIG_SYS_SDRAM_BASE;
	gd->bd->bi_dram[0].size = get_effective_memsize();
#endif

	mem_map_fill();

	return 0;
}

int dram_init(void)
{
	gd->ram_size = get_ram_size((void *)CONFIG_SYS_SDRAM_BASE,
				    CONFIG_SYS_SDRAM_SIZE);

	return 0;
}
#endif

void reset_cpu(ulong addr)
{
}

#if defined(CONFIG_BOARD_LATE_INIT)
static const struct {
	u32 bit;
	const char *name;
} reset_reasons[] = {
	{ RESET_REASON_DEBUG_SYS, "DEBUG" },
	{ RESET_REASON_SOFT, "SOFT" },
	{ RESET_REASON_SRST, "SRST" },
	{ RESET_REASON_PSONLY, "PS-ONLY" },
	{ RESET_REASON_PMU, "PMU" },
	{ RESET_REASON_INTERNAL, "INTERNAL" },
	{ RESET_REASON_EXTERNAL, "EXTERNAL" },
	{}
};

static int reset_reason(void)
{
	u32 reg;
	int i, ret;
	const char *reason = NULL;

	ret = zynqmp_mmio_read((ulong)&crlapb_base->reset_reason, &reg);
	if (ret)
		return -EINVAL;

	puts("Reset reason:\t");

	for (i = 0; i < ARRAY_SIZE(reset_reasons); i++) {
		if (reg & reset_reasons[i].bit) {
			reason = reset_reasons[i].name;
			printf("%s ", reset_reasons[i].name);
			break;
		}
	}

	puts("\n");

	env_set("reset_reason", reason);

	ret = zynqmp_mmio_write(~0, ~0, (ulong)&crlapb_base->reset_reason);
	if (ret)
		return -EINVAL;

	return ret;
}

static int set_fdtfile(void)
{
	char *compatible, *fdtfile;
	const char *suffix = ".dtb";
	const char *vendor = "xilinx/";

	if (env_get("fdtfile"))
		return 0;

	compatible = (char *)fdt_getprop(gd->fdt_blob, 0, "compatible", NULL);
	if (compatible) {
		debug("Compatible: %s\n", compatible);

		/* Discard vendor prefix */
		strsep(&compatible, ",");

		fdtfile = calloc(1, strlen(vendor) + strlen(compatible) +
				 strlen(suffix) + 1);
		if (!fdtfile)
			return -ENOMEM;

		sprintf(fdtfile, "%s%s%s", vendor, compatible, suffix);

		env_set("fdtfile", fdtfile);
		free(fdtfile);
	}

	return 0;
}

int board_late_init(void)
{
	u32 reg = 0;
	u8 bootmode;
	struct udevice *dev;
	int bootseq = -1;
	int bootseq_len = 0;
	int env_targets_len = 0;
	const char *mode;
	char *new_targets;
	char *env_targets;
	int ret;

#if defined(CONFIG_USB_ETHER) && !defined(CONFIG_USB_GADGET_DOWNLOAD)
	usb_ether_init();
#endif

	if (!(gd->flags & GD_FLG_ENV_DEFAULT)) {
		debug("Saved variables - Skipping\n");
		return 0;
	}

	ret = set_fdtfile();
	if (ret)
		return ret;

	ret = zynqmp_mmio_read((ulong)&crlapb_base->boot_mode, &reg);
	if (ret)
		return -EINVAL;

	if (reg >> BOOT_MODE_ALT_SHIFT)
		reg >>= BOOT_MODE_ALT_SHIFT;

	bootmode = reg & BOOT_MODES_MASK;

	puts("Bootmode: ");
	switch (bootmode) {
	case USB_MODE:
		puts("USB_MODE\n");
		mode = "usb";
		env_set("modeboot", "usb_dfu_spl");
		break;
	case JTAG_MODE:
		puts("JTAG_MODE\n");
		mode = "pxe dhcp";
		env_set("modeboot", "jtagboot");
		break;
	case QSPI_MODE_24BIT:
	case QSPI_MODE_32BIT:
		mode = "qspi0";
		puts("QSPI_MODE\n");
		env_set("modeboot", "qspiboot");
		break;
	case EMMC_MODE:
		puts("EMMC_MODE\n");
		mode = "mmc0";
		env_set("modeboot", "emmcboot");
		break;
	case SD_MODE:
		puts("SD_MODE\n");
		if (uclass_get_device_by_name(UCLASS_MMC,
					      "mmc@ff160000", &dev) &&
		    uclass_get_device_by_name(UCLASS_MMC,
					      "sdhci@ff160000", &dev)) {
			puts("Boot from SD0 but without SD0 enabled!\n");
			return -1;
		}
		debug("mmc0 device found at %p, seq %d\n", dev, dev->seq);

		mode = "mmc";
		bootseq = dev->seq;
		env_set("modeboot", "sdboot");
		break;
	case SD1_LSHFT_MODE:
		puts("LVL_SHFT_");
		/* fall through */
	case SD_MODE1:
		puts("SD_MODE1\n");
		if (uclass_get_device_by_name(UCLASS_MMC,
					      "mmc@ff170000", &dev) &&
		    uclass_get_device_by_name(UCLASS_MMC,
					      "sdhci@ff170000", &dev)) {
			puts("Boot from SD1 but without SD1 enabled!\n");
			return -1;
		}
		debug("mmc1 device found at %p, seq %d\n", dev, dev->seq);

		mode = "mmc";
		bootseq = dev->seq;
		env_set("modeboot", "sdboot");
		break;
	case NAND_MODE:
		puts("NAND_MODE\n");
		mode = "nand0";
		env_set("modeboot", "nandboot");
		break;
	default:
		mode = "";
		printf("Invalid Boot Mode:0x%x\n", bootmode);
		break;
	}

	if (bootseq >= 0) {
		bootseq_len = snprintf(NULL, 0, "%i", bootseq);
		debug("Bootseq len: %x\n", bootseq_len);
	}

	/*
	 * One terminating char + one byte for space between mode
	 * and default boot_targets
	 */
	env_targets = env_get("boot_targets");
	if (env_targets)
		env_targets_len = strlen(env_targets);

	new_targets = calloc(1, strlen(mode) + env_targets_len + 2 +
			     bootseq_len);
	if (!new_targets)
		return -ENOMEM;

	if (bootseq >= 0)
		sprintf(new_targets, "%s%x %s", mode, bootseq,
			env_targets ? env_targets : "");
	else
		sprintf(new_targets, "%s %s", mode,
			env_targets ? env_targets : "");

	env_set("boot_targets", new_targets);

	reset_reason();

	return 0;
}
#endif

int checkboard(void)
{
	puts("Board: Xilinx ZynqMP\n");
	return 0;
}

#if defined(CONFIG_AES)

#define KEY_LEN				64
#define IV_LEN				24
#define ZYNQMP_SIP_SVC_PM_SECURE_LOAD	0xC2000019
#define ZYNQMP_PM_SECURE_AES		0x1

int aes_decrypt_hw(u8 *key_ptr, u8 *src_ptr, u8 *dst_ptr, u32 len)
{
	int ret;
	u32 src_lo, src_hi, wlen;
	u32 ret_payload[PAYLOAD_ARG_CNT];

	if ((ulong)src_ptr != ALIGN((ulong)src_ptr,
				    CONFIG_SYS_CACHELINE_SIZE)) {
		debug("FAIL: Source address not aligned:%p\n", src_ptr);
		return -EINVAL;
	}

	src_lo = (u32)(ulong)src_ptr;
	src_hi = upper_32_bits((ulong)src_ptr);
	wlen = DIV_ROUND_UP(len, 4);

	memcpy(src_ptr + len, key_ptr, KEY_LEN + IV_LEN);
	len = ROUND(len + KEY_LEN + IV_LEN, CONFIG_SYS_CACHELINE_SIZE);
	flush_dcache_range((ulong)src_ptr, (ulong)(src_ptr + len));

	ret = invoke_smc(ZYNQMP_SIP_SVC_PM_SECURE_LOAD, src_lo, src_hi, wlen,
			 ZYNQMP_PM_SECURE_AES, ret_payload);
	if (ret)
		printf("Fail: %s: %d\n", __func__, ret);

	return ret;
}
#endif

#ifdef CONFIG_MISC_INIT_R

#define MUCTPI_SWX_ADDRESS              0x71
#define MUCTPI_SWX_SELECT               0x08
#define MUCTPI_MAC_ADDRESS              0x57
#define MUCTPI_MAC_OFFSET               0x7a
#define MUCTPI_MAC_LENGTH               6

int misc_init_r(void) {

    uchar buf[6];
    char str1[18], str2[18];

    struct udevice *dev;
    int ret, len;
    char req[SIPL_MESSAGE_MAXSIZE], rsp[SIPL_MESSAGE_MAXSIZE];
    struct SIPL_IpmiMessage qmsg, rmsg;
    unsigned int hw_addr, ipmb_addr, site_number, site_type;
    char shelf_addr[SIPL_SHELF_ADDR_MAXSIZE+1];

    // activate serial device
    if((ret = sipl_activate(CONFIG_SIPL_SERIAL_DEVICE_NAME,&dev))) {
        printf("cannot find UCLASS_SERIAL device \"%s\"\n",CONFIG_SIPL_SERIAL_DEVICE_NAME);
    }
    else {
        printf("SIPL:  %s\n",dev->name);

#ifdef CONFIG_SIPL_DIAGNOSTICS
        strcpy(req,"[B0000100]");
        len = strlen(req);
        printf("-------------------- DIAG #1 --------------------\n");
        printf("SIPL-TM request =  \"%s\"\n",req);
        if((ret = sipl_tm_write(dev,req,&len))) {
            printf("ERROR: writing SIPL-TM request \"%s\" - %d\n",req,-ret);
        }
        else {
            len = 64;
            if((ret = sipl_tm_read(dev,rsp,&len))) {
                rsp[len] = '\0';
                printf("ERROR: reading SIPL-TM response \"%s\" - %d\n",rsp,-ret);
            }
            else {
                rsp[len] = '\0';
                printf("SIPL-TM response = \"%s\"\n",rsp);
           }
       }

        strcpy(req,"[B0000100]");
        len = strlen(req);
        printf("-------------------- DIAG #2 --------------------\n");
        printf("SIPL-TM request =  \"%s\"\n",req);
        if((ret = sipl_tm_command(dev,req,rsp,&len))) {
            printf("ERROR: running SIPL-TM command - %d\n",-ret);
        }
        else {
            rsp[len] = '\0';
            printf("SIPL-TM response = \"%s\"\n",rsp);
        }

        strcpy(req,"[18 00 34 00 20 B0 30 82 02 02 00 7A]");
        len = strlen(req);
        printf("-------------------- DIAG #3.1 ------------------\n");
        printf("SIPL-TM request =  \"%s\"\n",req);
        if((ret = sipl_tm_command(dev,req,rsp,&len))) {
            printf("ERROR: running SIPL-TM command - %d\n",-ret);
        }
        else {
            rsp[len] = '\0';
            printf("SIPL-TM response = \"%s\"\n",rsp);
        }

        strcpy(req,"[18 00 31]");
        len = strlen(req);
        printf("-------------------- DIAG #3.2 ------------------\n");
        printf("SIPL-TM request =  \"%s\"\n",req);
        if((ret = sipl_tm_command(dev,req,rsp,&len))) {
            printf("ERROR: running SIPL-TM command - %d\n",-ret);
        }
        else {
            rsp[len] = '\0';
            printf("SIPL-TM response = \"%s\"\n",rsp);
        }

        strcpy(req,"[18 00 33]");
        len = strlen(req);
        printf("-------------------- DIAG #3.2 ------------------\n");
        printf("SIPL-TM request =  \"%s\"\n",req);
        if((ret = sipl_tm_command(dev,req,rsp,&len))) {
            printf("ERROR: running SIPL-TM command - %d\n",-ret);
        }
        else {
            rsp[len] = '\0';
            printf("SIPL-TM response = \"%s\"\n",rsp);
        }

        sipl_ipmi_message_init(qmsg);
        sipl_ipmi_message_init(rmsg);
        qmsg.NetFn = SIPL_NETFN_APP_REQUEST;
        qmsg.cmd = SIPL_CMD_APP_GET_MSG_FLAGS;
        printf("-------------------- DIAG #4.1 ------------------\n");
        sipl_ipmi_message_print(qmsg,"IPMI request:");
        if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
            printf("ERROR: running IPMI command - %d\n",-ret);
        }
        else {
            sipl_ipmi_message_print(rmsg,"IPMI response:");
        }

        printf("-------------------- DIAG #4.2 ------------------\n");
        qmsg.cmd = SIPL_CMD_APP_GET_MESSAGE;
        sipl_ipmi_message_print(qmsg,"IPMI request:");
        if((ret = sipl_ipmi_command(dev,qmsg,&rmsg))) {
            printf("ERROR: running IPMI command - %d\n",-ret);
        }
        else {
            sipl_ipmi_message_print(rmsg,"IPMI response:");
        }

        qmsg.rsSA = SIPL_RSSA_SHMGR;
        qmsg.rqSA = 0x82;
        qmsg.NetFn = SIPL_NETFN_PICMG_REQUEST;
        qmsg.cmd = SIPL_CMD_PICMG_GET_SHMGR_ADDR;
        qmsg.data[qmsg.dataLength++] = SIPL_DATA_PICMG_IDENTIFIER;
        printf("-------------------- DIAG #5 --------------------\n");
        sipl_ipmi_message_print(qmsg,"SendMessage request:");
        if((ret = sipl_send_message_command(dev,qmsg,&rmsg))) {
        }
        else {
            sipl_ipmi_message_print(rmsg,"SendMessage response:");
        }
#endif  /* CONFIG_SIPL_DIAGNOSTICS */

        ret = 0;
        if(sipl_get_address_info(dev,&hw_addr,&ipmb_addr,&site_number,&site_type)) {
            printf("ERROR: reading IPMI address info\n");
            ret = -1;
        }
        if(sipl_get_shelf_address_info(dev, ipmb_addr, shelf_addr, &len)) {
            printf("ERROR: reading IPMI shelf address info\n");
            ret = -1;
        }
        if(!ret) {
			shelf_addr[len+1] = '\0';
            printf("IPMC:  addr = %02X, site = (%02X,%02X), shelf = \"%s\"\n",ipmb_addr,site_type,site_number,shelf_addr);
        }
    }

    /* write selection to I2C switch */
    buf[0] = MUCTPI_SWX_SELECT;
    if (i2c_write(MUCTPI_SWX_ADDRESS,MUCTPI_SWX_SELECT,1,buf,1) != 0) {
        printf("MAC:   ERROR - writing selection to I2C switch\n");
        return(0);
    }

    /* read MAC address from I2C EEPROM */
    if (i2c_read(MUCTPI_MAC_ADDRESS,MUCTPI_MAC_OFFSET,1,buf,MUCTPI_MAC_LENGTH) != 0) {
        printf("MAC:   ERROR - reading MAC address from I2C EEPROM\n");
        return(0);
    }

    sprintf(str1,"%02x:%02x:%02x:%02x:%02x:%02x",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
    if(!is_valid_ethaddr(buf)) {
        printf("MAC:   ERROR - invalid MAC address from I2C EEPROM: \"%s\"\n",str1);
        return(0);
    }

    env_set("eth1addr",str1);
    sprintf(str2,"%02x:%02x:%02x:%02x:%02x:%02x",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]+1);
    env_set("eth2addr",str2);
    printf("MAC:   I2C EEPROM: eth1 %s, eth2 %s\n",str1,str2);

    return(0);
}
#endif
