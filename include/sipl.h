/* --------------------------------------------------------------------------
 * file: include/sipl.h
 * desc: SIPL-TM protocol defines and declarations
 * auth: 15-JUL-2020 R. Spiwoks
 * -------------------------------------------------------------------------- */

#ifdef SIPL_DEBUG
#define sipl_debug(fmt,args...) printf(fmt,##args)
#define sipl_debug_message(x,y) sipl_ipmi_message_print(x,y)
#else
#define sipl_debug(fmt,args...)
#define sipl_debug_message(x,y)
#endif /* SIPL_DEBUG */


#define SIPL_MESSAGE_MAXSIZE                 256
#define SIPL_READ_TIMEOUT                  10000
#define SIPL_RESPONSE_TIMEOUT                100

#define SIPL_NETFN_APP_REQUEST              0x06
#define SIPL_NETFN_PICMG_REQUEST            0x2C
#define SIPL_NETFN_RESPONSE_MASK               1

#define SIPL_CMD_APP_GET_MSG_FLAGS          0x31
#define SIPL_CMD_APP_GET_MESSAGE            0x33
#define SIPL_CMD_APP_SEND_MESSAGE           0x34
#define SIPL_CMD_PICMG_GET_ADDR             0x01
#define SIPL_CMD_PICMG_GET_SHMGR_ADDR       0x02

#define SIPL_CMD_APP_GET_ADDR_KEY_IPMB      0x01

#define SIPL_DATA_PICMG_IDENTIFIER          0x00

#define SIPL_IPMI_TYPE_MASK                 0xC0
#define SIPL_IPMI_TYPE_LANGCODE             0xC0
#define SIPL_IPMI_LENGTH_MASK               0x3F
#define SIPL_SHELF_ADDR_MAXSIZE               20

#define SIPL_RSSA_SHMGR                     0x20
#define SIPL_RQLUN_SEND_MESSAGE                2
#define SIPL_SEND_MESSAGE_MINSIZE              8
#define SIPL_GET_MESSAGE_TIMEOUT             300
#define SIPL_GET_MESSAGE_AVAILABLE          0x01

typedef struct cmd_tbl cmd_tbl_t;

/* -------------------------------------------------------------------------- */

int sipl_activate(const char *name, struct udevice **devp);

/* -------------------------------------------------------------------------- */

int  sipl_tm_write(struct udevice *dev, const char *req, int *len);
int  sipl_tm_read(struct udevice *dev, char *rsp, int* len);
int  sipl_tm_command(struct udevice *dev, char *req, char *rsp, int* len);

/* -------------------------------------------------------------------------- */

struct SIPL_IpmiMessage {

    unsigned char       rsSA;
    unsigned char       NetFn;
    unsigned char       rsLUN;
    unsigned char       rqSA;
    unsigned char       rqSeq;
    unsigned char       rqLUN;
    unsigned char       cmd;
    unsigned char       completionCode;
    unsigned char       dataLength;
    unsigned char       data[SIPL_MESSAGE_MAXSIZE];
};

#define sipl_ipmi_message_init(x) memset(&x,0,sizeof(struct SIPL_IpmiMessage))
#define sipl_ipmi_message_checksum(x) (((~x) + 1) & 0xff)
void sipl_ipmi_message_print(const struct SIPL_IpmiMessage msg, const char *str);

/* -------------------------------------------------------------------------- */

int  sipl_ipmi_write_request(const struct SIPL_IpmiMessage msg, char *req, int *len);
int  sipl_ipmi_read_byte(const char byt[2], unsigned int *num);
void sipl_ipmi_print_error(const char *rsp, const int len, const char *err);
int  sipl_ipmi_read_response(const char *rsp, struct SIPL_IpmiMessage *msg, int *len);
int  sipl_ipmi_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg);

/* -------------------------------------------------------------------------- */

int  sipl_send_message_write_request(struct SIPL_IpmiMessage *smsg, const struct SIPL_IpmiMessage msg);
int  sipl_get_message_read_response(const struct SIPL_IpmiMessage tmsg, struct SIPL_IpmiMessage *msg);
int  sipl_send_message_command(struct udevice *dev, const struct SIPL_IpmiMessage qmsg, struct SIPL_IpmiMessage *rmsg);

/* -------------------------------------------------------------------------- */

int  sipl_get_address_info(struct udevice *dev, unsigned int *hw_addr, unsigned int *ipmb_addr, unsigned int *site_number, unsigned int* site_type);
int sipl_get_address_info_sh_mgr(struct udevice *dev, const unsigned int ipmb_addr, unsigned int *site_number, unsigned int *site_type);
int  sipl_get_shelf_address_info(struct udevice *dev, const unsigned int ipmb_addr, char *shelf_addr, int *shelf_addr_len);
